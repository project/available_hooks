<?php

/**
 * @file
 * Template file for hook detail page.
 */
?>

<?php foreach ($data as $key => $value): ?>
  <div class="hook-info-list">
      <?php $hook_title = is_array($value) ? $value[1] : $value; ?>
      <H2><?php print str_replace('*', '', $hook_title); ?></H2>
      <div class="details">
          <?php if (is_array($value)): ?>    
            <?php foreach ($value as $line): ?>
              <pre>
                  <?php print $line; ?>
              </pre>
            <?php endforeach; ?>
          <?php endif; ?>
      </div>
  </div>    
<?php endforeach; ?>
