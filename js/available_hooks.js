/**
 * @file
 * JavaScript file for available_hooks module.
 */

(function ($, Drupal, window, document) {
  'use strict';
  Drupal.behaviors.available_hooks = {
    attach: function (context, settings) {
      $(document).ready(function () {
        $('.hook-info-list h2').click(function () {
          $(this).toggleClass('active').next().slideToggle();
        });
      });
    }
  };
})(jQuery, Drupal, this, this.document);
