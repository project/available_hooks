CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Maintainers



INTRODUCTION
------------

  You need Available Hooks if :

    * You are a Drupaler and love to write custom modules.
    * You are a Drupal newbie and don't know about hooks.
    * You don't know which modules provide hooks to extend their functionality.
    * You don't have internet to search for available hooks by other modules.

Available Hooks module provide all hooks from currently installed modules into
your project.


REQUIREMENTS
------------

  This module does not have any dependencies.


INSTALLATION
------------

  * Install as you would normally install a contributed Drupal module. See:
    https://drupal.org/documentation/install/modules-themes/modules-7
    for further information.

CONFIGURATION
-------------

  * Configure user permissions in Administration » People » Permissions:

    - administer available_hooks module

      User with above permission can see the hook listing page.

  * Hook listing page in
    Administration » Configuration » Development » Available Hooks:

    - This page will list all module those are providing own hooks.
    - On clicking an individual modules, all hooks provided by that module will
    - display with accordion effect.


MAINTAINERS
-----------

  Current maintainers:
    * Yogesh Kushwaha - https://www.drupal.org/u/yogeshkushwaha89        
    * Avanish Singh - https://www.drupal.org/u/avanish.singh
