<?php

/**
 * @file
 * Includes file for hook listing page.
 */

/**
 * Display module name who has implemented their own hooks.
 *
 * @return array
 *   Return module list as ordered array.
 */
function available_hooks_view_list() {
  // Cached all module hooks.
  $data = available_hooks_list_all();

  return array(
    '#theme' => 'item_list',
    '#items' => $data,
    '#type' => 'ul',
    '#attributes' => array('class' => 'available-hooks-module-list'),
  );
}

/**
 * List all hooks of single module.
 *
 * @param string $module_name
 *   Name of the module who's hooks will be list.
 *
 * @return string
 *   Themes html will be return as string.
 */
function available_hooks_view_module_hook($module_name) {
  // Get module hooks from cache.
  $module_name = check_plain($module_name);
  $output = array();
  $cache = cache_get('available_hooks_' . $module_name, 'cache');
  if ($cache && !empty($cache->data)) {
    $data = $cache->data;
  }
  else {
    $module_path = drupal_get_path('module', $module_name);
    $file_name = $module_name . '.api.php';
    $data = available_hooks_cache_list($module_path . '/' . $file_name, $module_name);
  }

  // Prepare line by line array code to display hooks.
  $data = current($data);
  if (!$data) {
    $data[] = t('Hook definition not found.');
  }
  foreach ($data as $hooks) {
    $output[] = $hooks;
  }

  return array(
    array(
      '#theme' => 'available_hooks_detail_page',
      '#data' => $output,
      '#attached' => array(
        'css' => array(
          drupal_get_path('module', 'available_hooks') . '/css/available_hooks.css',
        ),
        'js' => array(
          drupal_get_path('module', 'available_hooks') . '/js/available_hooks.js',
        ),
      ),
    ),
  );
}
